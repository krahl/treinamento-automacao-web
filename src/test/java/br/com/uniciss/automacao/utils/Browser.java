package br.com.uniciss.automacao.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Browser {
    static WebDriver driver = null;
    static WebDriverWait wait = null;

    public static WebDriver getDriver(String descricao){
        switch (descricao){
            case "chrome":
                System.setProperty("webdriver.chrome.driver","/chromedriver");
                driver = new ChromeDriver();
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver","/geckodriver");
                driver = new FirefoxDriver();
                break;
        }
        wait = new WebDriverWait(driver,10);
        return driver;
    }
    public static WebDriverWait getWait(){
        return wait;
    }

    public static void fechaNavegador(){
        driver.quit();
    }
}
