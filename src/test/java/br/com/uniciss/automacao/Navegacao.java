package br.com.uniciss.automacao;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Navegacao {
    @Test
    public void testeNavegacao() {
        System.setProperty("webdriver.chrome.driver", "/chromedriver");
        WebDriver driver = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"navegacao.html");
        driver.navigate().to(url+"datahora.html");
        driver.navigate().refresh();
        driver.navigate().back();
        driver.navigate().forward();
        driver.quit();
    }
}