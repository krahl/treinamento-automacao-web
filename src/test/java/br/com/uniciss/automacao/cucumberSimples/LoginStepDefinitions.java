package br.com.uniciss.automacao.cucumberSimples;

import br.com.uniciss.automacao.utils.Browser;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class LoginStepDefinitions {
    private WebDriver driver;
    private WebDriverWait wait;
    private LoginPageObjects loginPageObjects;

    public LoginStepDefinitions(){
        driver = Browser.getDriver("chrome");
        wait = Browser.getWait();
        loginPageObjects = new LoginPageObjects(driver, wait);
    }

    @After
    public void after(){
        Browser.fechaNavegador();
    }

    @Dado("^que estou na tela de login\\.$")
    public void que_estou_na_tela_de_login(){
        loginPageObjects.acessaTelaLogin();

    }

    @Quando("^eu informar o campo usuário com \"([^\"]*)\"\\.$")
    public void eu_informar_o_campo_usuário_com(String usuario){
        loginPageObjects.informaUsuario(usuario);
    }

    @Quando("^eu informar o campo senha com \"([^\"]*)\"\\.$")
    public void eu_informar_o_campo_senha_com(String senha){
        loginPageObjects.informaSenha(senha);
    }

    @Quando("^eu clicar no botão Efetuar Login\\.$")
    public void eu_clicar_no_botão_Efetuar_Login(){
        loginPageObjects.clicaLogar();
    }

    @Então("^a mensagem \"([^\"]*)\" é apresentada\\.$")
    public void a_mensagem_é_apresentada(String mensagem){
        assertEquals(mensagem, loginPageObjects.capturaMensagem());
    }
}
