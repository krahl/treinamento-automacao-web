package br.com.uniciss.automacao.cucumberSimples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageObjects {
    private WebDriver driver;
    private WebDriverWait wait;
    private static final By campoUsuario = By.id("username");
    private static final By campoSenha = By.id("password");
    private static final By botaoLogin = By.id("login");
    private static final By msg = By.xpath("//div[contains(@id,'message')]/p");

    public LoginPageObjects(WebDriver driver, WebDriverWait wait){
        this.driver = driver;
        this.wait = wait;
    }

    public void acessaTelaLogin(){
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/cucumber.html");
    }

    public void informaUsuario(String usuario){
        driver.findElement(campoUsuario).sendKeys(usuario);
    }

    public void informaSenha(String senha){
        driver.findElement(campoSenha).sendKeys(senha);
    }

    public void clicaLogar(){
        driver.findElement(botaoLogin).click();
    }

    public String capturaMensagem(){
        wait.until(ExpectedConditions.presenceOfElementLocated(msg));
        return driver.findElement(msg).getText();

    }
}
