package br.com.uniciss.automacao.alertas;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class AlertasEConfirmacoesBDD {
    private WebDriver driver;
    private WebDriverWait wait;

    private void loginComFalha(String user, String password){
        driver.findElement(By.id("user")).sendKeys(user);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("submit")).click();

        Alert alert = driver.switchTo().alert();
        assertEquals("Preencha os dois campos!!!", alert.getText());
        alert.dismiss();
    }

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/alert_confirmation.html");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void loginSemSenha(){
        loginComFalha("admin","");
    }

    @Test
    public void loginSemUser(){
        loginComFalha("","admin");
    }

    @Test
    public void loginCerto(){
        driver.findElement(By.id("user")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("admin");
        driver.findElement(By.id("submit")).click();

        assertEquals("Cadastrar", driver.findElement(By.linkText("Cadastrar")).getText());
    }

    @Test
    public void cadastrar(){
        loginECadastro();

    }

    @Test
    public void alterar(){
        loginECadastro();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.tagName("table")));
        //driver.findElement(By.xpath("//td[text()='admin@admin.com.br']/../td[5]/a")).click();
        driver.findElement(By.xpath("//tr/td[4 and text()='admin@admin.com.br']/../td[5]/a")).click();

        WebElement nome = driver.findElement(By.name("nome"));
        nome.clear();
        nome.sendKeys("Krahl");

        WebElement sobrenome = driver.findElement(By.name("sobrenome"));
        sobrenome.clear();
        sobrenome.sendKeys("Krahl");

        WebElement email = driver.findElement(By.name("email"));
        email.clear();
        email.sendKeys("krahl.lucas@ciss.com.br");
        driver.findElement(By.id("add_remove")).click();

        Alert alert = driver.switchTo().alert();
        assertEquals("Usuário alterado com sucesso!", alert.getText());
        alert.dismiss();
    }

    @Test
    public void excluir(){
        loginECadastro();
        WebElement linha = driver.findElement(By.xpath("//td[text()='admin@admin.com.br']/.."));
        linha.findElement(By.cssSelector("a[href*='excluir.php']")).click();

        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    private void loginECadastro() {
        driver.findElement(By.id("user")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("admin");
        driver.findElement(By.id("submit")).click();

        driver.findElement(By.linkText("Cadastrar")).click();

        driver.findElement(By.name("nome")).sendKeys("Admin");
        driver.findElement(By.name("sobrenome")).sendKeys("admin");
        driver.findElement(By.name("email")).sendKeys("admin@admin.com.br");
        driver.findElement(By.id("add_remove")).click();

        Alert alert = driver.switchTo().alert();
        assertEquals("Usuário cadastrado com sucesso!", alert.getText());
        alert.dismiss();
    }

}
