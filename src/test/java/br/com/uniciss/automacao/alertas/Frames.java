package br.com.uniciss.automacao.alertas;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Frames {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/frames/frames.html");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void trocaDeFrames(){
        WebElement menu = driver.findElement(By.name("menu"));
        driver.switchTo().frame(menu);

        driver.findElement(By.linkText("Lojas de Materiais para Construção")).click();

        driver.switchTo().defaultContent();

        WebElement frame = driver.findElement(By.name("content"));
        driver.switchTo().frame(frame);

        //locator
        By locator = By.cssSelector("a[href*='ciss.com.br']");
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
        WebElement titulo = driver.findElement(locator);
        assertEquals("Início do site da CISS", titulo.getAttribute("title"));
    }

    @Test
    public void substituirFrame(){
        WebElement menu = driver.findElement(By.name("menu"));
        driver.switchTo().frame(menu);

        driver.findElement(By.linkText("Histórico")).click();

        assertEquals("404 | Página não encontrada", driver.getTitle());
    }

    @Test
    public void novaAba(){
        WebElement menu = driver.findElement(By.name("menu"));
        driver.switchTo().frame(menu);

        driver.findElement(By.linkText("Central de Atendimento")).click();

        String janelaAntiga = driver.getWindowHandle();
        Set<String> janelas = driver.getWindowHandles();

        for(String janela: janelas){
            driver.switchTo().window(janela);
            if(driver.getCurrentUrl().equals("https://ciss.com.br/atendimento")){
                break;
            }
        }

        assertTrue(driver.findElement(By.xpath("//a[contains(text(), 'Sobre a CISS')]")).isDisplayed());

        driver.close();
        driver.switchTo().window(janelaAntiga);
        driver.switchTo().frame(menu);
        driver.findElement(By.partialLinkText("Central de Atendimento")).click();

    }

}
