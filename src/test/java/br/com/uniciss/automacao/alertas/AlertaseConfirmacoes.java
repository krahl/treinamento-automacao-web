package br.com.uniciss.automacao.alertas;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class AlertaseConfirmacoes {
    private WebDriver driver;

    private void loginComFalha(String user, String password){
        driver.findElement(By.id("user")).sendKeys(user);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("submit")).click();

        Alert alert = driver.switchTo().alert();
        assertEquals("Preencha os dois campos!!!", alert.getText());
        alert.dismiss();
    }

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/alert_confirmation.html");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    @Ignore
    public void loginSemSenha(){
        loginComFalha("admin","");
    }

    @Test
    public void loginSemUser(){
        loginComFalha("","admin");
    }

}
