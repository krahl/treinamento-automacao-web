package br.com.uniciss.automacao.alertas;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class AlertasEConfirmacoes2 {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/alert_confirmation_js.html");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void alerta(){
        driver.findElement(By.id("alerta")).click();

        By element = By.cssSelector(".bootbox-body");
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));

        assertEquals("Este é um alerta", driver.findElement(element).getText());
        driver.findElement(By.cssSelector(".btn.btn-primary")).click();

    }

    @Test
    public void confirmacao(){
        By element = By.cssSelector(".bootbox-body");
        By msg = By.id("msg");

        driver.findElement(By.id("confirmacao")).click();

        //Aguarda o alerta
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        assertEquals("Tem certeza?", driver.findElement(element).getText());

        //Clica em ok
        driver.findElement(By.cssSelector(".btn.btn-primary")).click();
        //Pega a mensagem
        wait.until(ExpectedConditions.visibilityOfElementLocated(msg));
        assertEquals("Aceitou", driver.findElement(By.xpath("//div[@id='msg']/span")) .getText());

        //Espera a mensagem sumir
        wait.until(ExpectedConditions.invisibilityOfElementLocated(msg));

        driver.findElement(By.id("confirmacao")).click();

        //Agarda p alerta
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        assertEquals("Tem certeza?", driver.findElement(element).getText());

        //Clica em cancelar
        driver.findElement(By.cssSelector(".btn.btn-default")).click();
        //Pega o conteudo da mensagem
        wait.until(ExpectedConditions.visibilityOfElementLocated(msg));
        assertEquals("Cancelou", driver.findElement(By.xpath("//div[@id='msg']/span")) .getText());
    }

    @Test
    public void prompt(){
        By element = By.cssSelector(".modal-title");
        By msg = By.id("msg");
        String nome = "Lucas Krahl";

        driver.findElement(By.id("prompt")).click();

        //Aguarda o alerta
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        assertEquals("Qual seu nome?", driver.findElement(element).getText());

        driver.findElement(By.cssSelector(".bootbox-input.bootbox-input-text.form-control")).sendKeys(nome);
        //Clica em ok
        driver.findElement(By.cssSelector(".btn.btn-primary")).click();

        //Pega a mensagem
        wait.until(ExpectedConditions.visibilityOfElementLocated(msg));
        assertEquals("Olá: "+nome, driver.findElement(By.xpath("//div[@id='msg']/span")) .getText());

        //Espera a mensagem sumir
        wait.until(ExpectedConditions.invisibilityOfElementLocated(msg));

        driver.findElement(By.id("prompt")).click();

        //Aguarda o alerta
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        assertEquals("Qual seu nome?", driver.findElement(element).getText());

        //Clica em cancelar
        driver.findElement(By.cssSelector(".btn.btn-default")).click();
        //Pega o conteudo da mensagem
        wait.until(ExpectedConditions.visibilityOfElementLocated(msg));
        assertEquals("Prompt fechado", driver.findElement(By.xpath("//div[@id='msg']/span")) .getText());

    }
}
