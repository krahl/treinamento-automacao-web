package br.com.uniciss.automacao.cucumberExemplos;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/exemplos.feature")
public class RunCucumberExemplos {

}
