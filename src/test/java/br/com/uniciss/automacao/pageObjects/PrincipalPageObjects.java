package br.com.uniciss.automacao.pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PrincipalPageObjects {
    private WebDriver driver;
    public static final By botaoCadastrar = By.linkText("Cadastrar");

    public PrincipalPageObjects(WebDriver driver){
        this.driver = driver;
    }

    public void clicaCadastrar(){
        driver.findElement(botaoCadastrar).click();
    }

    public void clicaAlterar(String email){
        WebElement linha = driver.findElement(By.xpath("//td[text()='"+email+"']/.."));
        linha.findElement(By.cssSelector("a[href*='alterar.php']")).click();
    }

    public void clicaExcluir(String email){
        WebElement linha = driver.findElement(By.xpath("//td[text()='"+email+"']/.."));
        linha.findElement(By.cssSelector("a[href*='excluir.php']")).click();
    }

    public void capturaMensagemExclusao() {
        Alert alerta = driver.switchTo().alert();
        alerta.accept();
    }
}
