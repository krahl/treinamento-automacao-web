package br.com.uniciss.automacao.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPageObjects {
    private WebDriver driver;
    private static final By campoUsuario = By.id("user");
    private static final By campoSenha= By.id("password");
    private static final By botaoLogin = By.id("submit");

    public LoginPageObjects(WebDriver driver){
        this.driver= driver;
    }

    public void abreTelaLogin(){
        driver.get("http://172.16.108.26:8081/ciss/selenium/html/alert_confirmation.html");
    }


    public void preencheUsuario(String usuario){
        driver.findElement(campoUsuario).sendKeys(usuario);
    }

    public void preencheSenha(String senha){
        driver.findElement(campoSenha).sendKeys(senha);
    }

    public void logar(){
        driver.findElement(botaoLogin).click();
    }

}
