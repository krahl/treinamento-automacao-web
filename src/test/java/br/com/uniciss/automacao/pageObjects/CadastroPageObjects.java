package br.com.uniciss.automacao.pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CadastroPageObjects {
    private WebDriver driver;
    private static final By campoNome = By.name("nome");
    private static final By campoSobrenome= By.name("sobrenome");
    private static final By campoEmail = By.name("email");
    private static final By botaoCadastrar = By.id("add_remove");

    public CadastroPageObjects(WebDriver driver){
        this.driver= driver;
    }

    public void preencheNome(String nome){
        driver.findElement(campoNome).sendKeys(nome);
    }

    public void preencheSobrenome(String sobrenome){
        driver.findElement(campoSobrenome).sendKeys(sobrenome);
    }

    public void preencheEmail(String email){
        driver.findElement(campoEmail).sendKeys(email);
    }

    public void clicaCadastrar(){
        driver.findElement(botaoCadastrar).click();
    }

    public String capturaMensagemGravacao(){
        Alert alerta = driver.switchTo().alert();
        String msg = alerta.getText();
        alerta.accept();
        return msg;
    }


}
