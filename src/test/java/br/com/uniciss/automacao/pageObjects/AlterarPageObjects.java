package br.com.uniciss.automacao.pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AlterarPageObjects {
    private WebDriver driver;
    private static final By campoNome = By.name("nome");
    private static final By campoSobrenome= By.name("sobrenome");
    private static final By campoEmail = By.name("email");
    private static final By botaoAlterar = By.id("add_remove");

    public AlterarPageObjects(WebDriver driver){
        this.driver= driver;
    }

    public void preencheNome(String nome){
        driver.findElement(campoNome).clear();
        driver.findElement(campoNome).sendKeys(nome);
    }

    public void preencheSobrenome(String sobrenome){
        driver.findElement(campoSobrenome).clear();
        driver.findElement(campoSobrenome).sendKeys(sobrenome);
    }

    public void preencheEmail(String email){
        driver.findElement(campoEmail).clear();
        driver.findElement(campoEmail).sendKeys(email);
    }

    public void clicaAlterar(){
        driver.findElement(botaoAlterar).click();
    }

    public String capturaMensagemGravacao(){
        Alert alerta = driver.switchTo().alert();
        String msg = alerta.getText();
        alerta.accept();
        return msg;
    }
}
