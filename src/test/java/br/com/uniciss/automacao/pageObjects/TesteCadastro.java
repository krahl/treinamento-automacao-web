package br.com.uniciss.automacao.pageObjects;

import br.com.uniciss.automacao.pageObjects.LoginPageObjects;
import br.com.uniciss.automacao.utils.Browser;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class TesteCadastro {
    private WebDriver driver;
    LoginPageObjects loginPageObjects;
    PrincipalPageObjects principalPageObjects;
    CadastroPageObjects cadastroPageObjects;
    AlterarPageObjects alterarPageObjects;

    @Before
    public void setUp(){
        driver = Browser.getDriver("chrome");
        loginPageObjects = new LoginPageObjects(driver);
        principalPageObjects = new PrincipalPageObjects(driver);
        cadastroPageObjects = new CadastroPageObjects(driver);
        alterarPageObjects = new AlterarPageObjects(driver);
    }

    @After
    public void tearDown(){
        Browser.fechaNavegador();
    }

    @Test
    public void testeCadastro(){
        cadastrarUsuario();
    }

    @Test
    public void alterarCadastro(){
        cadastrarUsuario();

        principalPageObjects.clicaAlterar("lucas.krahl@ciss.com.br");
        alterarPageObjects.preencheNome("Alterado por");
        alterarPageObjects.preencheSobrenome("Lucas");
        alterarPageObjects.preencheEmail("krahl.lucas@ciss.com.br");
        alterarPageObjects.clicaAlterar();
        assertEquals("Usuário alterado com sucesso!", alterarPageObjects.capturaMensagemGravacao());



    }

    @Test
    public void excluirCadastro(){
        cadastrarUsuario();

        principalPageObjects.clicaExcluir("lucas.krahl@ciss.com.br");
        principalPageObjects.capturaMensagemExclusao();

    }

    private void cadastrarUsuario() {
        loginPageObjects.abreTelaLogin();
        loginPageObjects.preencheUsuario("admin");
        loginPageObjects.preencheSenha("admin");
        loginPageObjects.logar();

        principalPageObjects.clicaCadastrar();
        cadastroPageObjects.preencheNome("Lucas Alexandre");
        cadastroPageObjects.preencheSobrenome("Krahl");
        cadastroPageObjects.preencheEmail("lucas.krahl@ciss.com.br");
        cadastroPageObjects.clicaCadastrar();
        assertEquals("Usuário cadastrado com sucesso!", cadastroPageObjects.capturaMensagemGravacao());
    }
}
