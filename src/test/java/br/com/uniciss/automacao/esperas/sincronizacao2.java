package br.com.uniciss.automacao.esperas;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class sincronizacao2 {
    private WebDriver driver;
    private WebDriverWait wait;

    @Test
    public void testeEspera(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("http://172.16.108.26:8081/ciss/selenium/html/ajax_show.html");

        driver.findElement(By.name("nome_aluno")).sendKeys("Jose");
        driver.findElement(By.name("email_aluno")).sendKeys("jose.jose@jose.jose.br.com");
        driver.findElement(By.id("idade")).click();
        Select comboIdade = new Select(driver.findElement(By.name("idade_aluno")));
        comboIdade.selectByValue("13");

        driver.findElement(By.name("nome_pais")).sendKeys("Jose Father");
        driver.findElement(By.name("email_pais")).sendKeys("father.jose@jose.father.br.com");

        driver.quit();
    }
}
