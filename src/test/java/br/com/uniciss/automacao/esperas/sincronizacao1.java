package br.com.uniciss.automacao.esperas;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class sincronizacao1 {
    private WebDriver driver;
    private WebDriverWait wait;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);

        driver.get("http://172.16.108.26:8081/ciss/selenium/html/ajax_loading.html");
    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void testeLoginSucesso(){
        driver.findElement(By.id("username")).sendKeys("demo");
        driver.findElement(By.id("password")).sendKeys("demo");
        driver.findElement(By.id("login")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("success")));

        assertEquals("Voce efetuou o login com sucesso!", driver.findElement(By.className("success")).getText());

    }
    @Test
    public void testeLoginFalha(){
        driver.findElement(By.id("username")).sendKeys("demo");
        driver.findElement(By.id("password")).sendKeys("dem2o");
        driver.findElement(By.id("login")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("error")));

        assertEquals("Usuario ou senhas invalidos.", driver.findElement(By.className("error")).getText());
    }
}
