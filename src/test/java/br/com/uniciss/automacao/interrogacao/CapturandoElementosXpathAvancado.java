package br.com.uniciss.automacao.interrogacao;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CapturandoElementosXpathAvancado {
    @Test
    public void capturandoElementosXpathAvancado(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        WebDriver driver  = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"css_xpath.html");


        driver.quit();
    }
}
