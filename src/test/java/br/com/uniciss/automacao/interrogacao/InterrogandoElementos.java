package br.com.uniciss.automacao.interrogacao;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class InterrogandoElementos {
    @Test
    public void capturandoElementosTest() {
        System.setProperty("webdriver.chrome.driver", "/chromedriver");
        WebDriver driver = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"elementos.html");

        WebElement email = driver.findElement(By.id("email"));
        WebElement senha = driver.findElement(By.name("pwd"));
        WebElement btnLogin = driver.findElement(By.name("send"));
        WebElement btnReset = driver.findElement(By.id("limpar"));
        WebElement lembrarSenha = driver.findElement(By.id("lembrar"));
        WebElement titulo = driver.findElement(By.tagName("h1"));
        WebElement voltar = driver.findElement(By.linkText("Voltar para a lista de exercícios"));
        driver.quit();
    }

}
