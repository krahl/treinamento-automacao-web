package br.com.uniciss.automacao.interrogacao;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CapturandoElementosXpathBaisco {
    @Test
    public void capturarElementosComXpathBasico(){
        System.setProperty("webdriver.chrome.driver","/chromedriver");
        WebDriver driver  = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"find_elements.html");

        WebElement divPai = driver.findElement(By.xpath("//div[@class='divpai border']"));
        WebElement divFilho = driver.findElement(By.xpath("//div[@class='divfilho border']"));
        WebElement divNeto1 = driver.findElement(By.xpath("//div[@class='divneto border'][1]"));
        WebElement divNeto2 = driver.findElement(By.xpath("//div[@class='divneto border'][2]"));
        WebElement dataInicio = driver.findElement(By.xpath("//input[@id='dataInicio']"));

        driver.quit();
    }
}
