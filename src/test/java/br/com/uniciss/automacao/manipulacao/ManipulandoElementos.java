package br.com.uniciss.automacao.manipulacao;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;

public class ManipulandoElementos {
    @Test
    public void testeManupulandoElementos(){
        System.setProperty("webdriver.chrome.driver", "/chromedriver");
        WebDriver driver = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"elementos_html.html");

        driver.findElement(By.name("fname")).sendKeys("João");
        driver.findElement(By.name("lname")).sendKeys("dos Santos");

        driver.findElement(By.cssSelector("input[value='Homem']")).click();

        driver.findElement(By.cssSelector("input[value='Carne']")).click();
        driver.findElement(By.cssSelector("input[value='Frango']")).click();
        driver.findElement(By.cssSelector("input[value='Pizza']")).click();

        driver.findElement(By.name("area")).clear();
        driver.findElement(By.name("area")).sendKeys("churrasco");

        Select comboEducation = new Select(driver.findElement(By.name("education")));
        comboEducation.selectByValue("graduado");

        Select comboDia = new Select(driver.findElement(By.id("dia")));
        comboDia.selectByValue("Manhã");
        comboDia.selectByValue("Noite");

        driver.findElement(By.name("send")).click();

        assertEquals("Olá João dos Santos",driver.findElement(By.id("nome")).getText());

        assertEquals("Você é Homem",driver.findElement(By.id("sexo")).getText());
        assertEquals("E você gosta de: Carne Frango Pizza",driver.findElement(By.id("comida")).getText());
        assertEquals("Você também gosta de: churrasco",driver.findElement(By.id("outra_comida")).getText());
        assertEquals("Sua parte favorita do dia é: Manhã Noite",driver.findElement(By.id("parte_favorita")).getText());
        assertEquals("Seu grau de instrução é: Graduado",driver.findElement(By.id("instrucao")).getText());
        driver.quit();
    }
}
