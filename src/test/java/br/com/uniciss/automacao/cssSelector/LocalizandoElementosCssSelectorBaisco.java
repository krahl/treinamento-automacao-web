package br.com.uniciss.automacao.cssSelector;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocalizandoElementosCssSelectorBaisco {
    @Test
    public void localizandoElementos() {
        System.setProperty("webdriver.chrome.driver", "/chromedriver");
        WebDriver driver = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"find_elements.html");

        WebElement email = driver.findElement(By.xpath("//input[starts-with(@id,'name-')]"));
        WebElement senha = driver.findElement(By.xpath("//input[ends-with(@id,'-password')]"));
        WebElement repetirSenha = driver.findElement(By.xpath("//input[contains(@id,'-password-')"));

        WebElement btnSubmit = driver.findElement(By.xpath("//input[is(@type,'submit')"));

        driver.quit();
    }
}
