package br.com.uniciss.automacao.cssSelector;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocalizandoElementosCssSelectorAvancado {
    @Test
    public void localizarElementosPorCssAvancado() {
        System.setProperty("webdriver.chrome.driver", "/chromedriver");
        WebDriver driver = new ChromeDriver();

        String url =  "http://172.16.108.26:8081/ciss/selenium/html/";
        driver.get(url+"css_xpath.html");

        WebElement email = driver.findElement(By.cssSelector("input[id^='name-']"));
        WebElement senha = driver.findElement(By.cssSelector("input[name$='-password']"));
        WebElement repetirSenha = driver.findElement(By.cssSelector("input[name*='-password-']"));

        WebElement btnSubmit = driver.findElement(By.cssSelector("input[type='submit']"));
        driver.quit();
    }

}
