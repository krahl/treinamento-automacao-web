#language: pt
#encoding: utf-8

Funcionalidade: Login Data Driven.
Esquema do Cenário: Efetuar login.
Dado que estou na tela de login.
Quando eu informar o usuário com "<usuario>".
E eu informar a senha com "<senha>".
E eu clicar no botão Efetuar Login.
Então a mensagem "<resultado>" é apresentada.

Exemplos:
|    usuario  |    senha    |             resultado              |
|  usuario01  |  usuario01  |  Voce efetuou o login com sucesso! |
|  usuario02  |  usuario02  |  Voce efetuou o login com sucesso! |
|  usuario03  |  usuario03  |  Voce efetuou o login com sucesso! |
|  usuario04  |  usuario04  |  Voce efetuou o login com sucesso! |
|             |  usuario02  |     Usuario ou senhas invalidos.    |
|  usuario04  |    teste    |     Usuario ou senhas invalidos.    |
|    teste    |    teste    |     Usuario ou senhas invalidos.    |
|  usuario01  |             |     Usuario ou senhas invalidos.    |