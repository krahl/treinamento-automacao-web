#language: pt
#encoding: utf-8

Funcionalidade: Login Cucumber

  Cenario: Login válido
    Dado que estou na tela de login.
    Quando eu informar o campo usuário com "usuario01".
    E eu informar o campo senha com "usuario01".
    E eu clicar no botão Efetuar Login.
    Então a mensagem "Voce efetuou o login com sucesso!" é apresentada.

  Cenario: Login válido
    Dado que estou na tela de login.
    Quando eu informar o campo usuário com "usuario02".
    E eu informar o campo senha com "usuario02".
    E eu clicar no botão Efetuar Login.
    Então a mensagem "Voce efetuou o login com sucesso!" é apresentada.

  Cenario: Login Inválido
    Dado que estou na tela de login.
    Quando eu informar o campo usuário com "usuario01".
    E eu informar o campo senha com "usuario02".
    E eu clicar no botão Efetuar Login.
    Então a mensagem "Usuario ou senhas invalidos." é apresentada.